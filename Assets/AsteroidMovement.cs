﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour {
    
    private float speed;

	// Use this for initialization
	void Start () {
        speed = 5;
	}
	
	// Update is called once per frame
	void Update () {
        float step = speed * Time.deltaTime;
        //transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("Main Camera").transform.position, step);
        transform.Translate(Vector3.back * step);
    }
}
