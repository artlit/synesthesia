﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAsteroid : MonoBehaviour {

    private float angularVelocity = 10.0f;
    private Vector3 axisOfRotation;

	// Use this for initialization
	void Start () {
        axisOfRotation = Random.onUnitSphere * 1;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(axisOfRotation, angularVelocity * Time.deltaTime * 2);
    }
}
