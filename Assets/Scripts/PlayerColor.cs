﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class PlayerColor : MonoBehaviour {

    //Gradient Colors
    Color32 red1 = new Color32(255, 0, 0, 1);
    Color32 red2 = new Color32(255, 100, 50, 1);
    Color32 red3 = new Color32(175, 0, 0, 1);
    Color32 red4 = new Color32(255, 50, 100, 1);
    Color32 red5 = new Color32(85, 0, 0, 1);

    Color32 yellow1 = new Color32(255, 255, 0, 1);
    Color32 yellow2 = new Color32(255, 150, 50, 1);
    Color32 yellow3 = new Color32(185, 185, 0, 1);
    Color32 yellow4 = new Color32(255, 255, 50, 1);
    Color32 yellow5 = new Color32(125, 125, 0, 1);

    Color32 blue1 = new Color32(0, 0, 255, 1);
    Color32 blue2 = new Color32(50, 100, 255, 1);
    Color32 blue3 = new Color32(0, 0, 175, 1);
    Color32 blue4 = new Color32(100, 50, 255, 1);
    Color32 blue5 = new Color32(0, 0, 85, 1);

    Color32 magenta1 = new Color32(255, 0, 255, 1);
    Color32 magenta2 = new Color32(255, 50, 50, 1);
    Color32 magenta3 = new Color32(175, 0, 175, 1);
    Color32 magenta4 = new Color32(50, 50, 255, 1);
    Color32 magenta5 = new Color32(85, 0, 85, 1);

    Color32 green1 = new Color32(0, 255, 0, 1);
    Color32 green2 = new Color32(175, 255, 50, 1);
    Color32 green3 = new Color32(0, 175, 0, 1);
    Color32 green4 = new Color32(100, 255, 175, 1);
    Color32 green5 = new Color32(0, 85, 0, 1);

    Color32 orange1 = new Color32(255, 125, 0, 1);
    Color32 orange2 = new Color32(255, 30, 0, 1);
    Color32 orange3 = new Color32(255, 85, 0, 1);
    Color32 orange4 = new Color32(255, 255, 0, 1);
    Color32 orange5 = new Color32(200, 85, 0, 1);

    //Warp Drive Streaks
    private ParticleSystem ps_1;
    private ParticleSystem ps;
    public float speed;
    public string asteroidNumber;

    private Vector3 velocity = Vector3.zero;
    //private float rangeSmooth = 0.0f;

    // Use this for initialization
    void Start () {
        speed = 5;
        //Audio
        AudioSource audioBack = GameObject.Find("Back").GetComponent<AudioSource>();
        audioBack.Play();
        AudioSource audioMain = GameObject.Find("Main").GetComponent<AudioSource>();
        audioMain.Play();
        //Warp Drive Streaks
        ps = GameObject.Find("WarpStreak").GetComponent<ParticleSystem>();
        ps_1 = GameObject.Find("WarpStreak_2").GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {
        float step = speed * Time.deltaTime;
        GameObject.Find("Main Camera").transform.Translate(Vector3.forward * step);
        //Warp Drive Streaks
        var main = ps.colorOverLifetime;
        var main_1 = ps_1.colorOverLifetime;
        main.enabled = true;
        main_1.enabled = true;
        //Audio
        AudioLowPassFilter lpfilter = GameObject.Find("Main").GetComponent(typeof(AudioLowPassFilter)) as AudioLowPassFilter;
        lpfilter.enabled = true;
        //Warp Drive Gradients
        Gradient redGrad = new Gradient();
        redGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(red1, 0.0f), new GradientColorKey(red2, 0.25f), new GradientColorKey(red3, 0.5f), new GradientColorKey(red4, 0.75f), new GradientColorKey(red5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });
        Gradient yellowGrad = new Gradient();
        yellowGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(yellow1, 0.0f), new GradientColorKey(yellow2, 0.25f), new GradientColorKey(yellow3, 0.5f), new GradientColorKey(yellow4, 0.75f), new GradientColorKey(yellow5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });
        Gradient blueGrad = new Gradient();
        blueGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(blue1, 0.0f), new GradientColorKey(blue2, 0.25f), new GradientColorKey(blue3, 0.5f), new GradientColorKey(blue4, 0.75f), new GradientColorKey(blue5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });
        Gradient magentaGrad = new Gradient();
        magentaGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(magenta1, 0.0f), new GradientColorKey(magenta2, 0.25f), new GradientColorKey(magenta3, 0.5f), new GradientColorKey(magenta4, 0.75f), new GradientColorKey(magenta5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });
        Gradient greenGrad = new Gradient();
        greenGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(green1, 0.0f), new GradientColorKey(green2, 0.25f), new GradientColorKey(green3, 0.5f), new GradientColorKey(green4, 0.75f), new GradientColorKey(green5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });
        Gradient orangeGrad = new Gradient();
        orangeGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(orange1, 0.0f), new GradientColorKey(orange2, 0.25f), new GradientColorKey(orange3, 0.5f), new GradientColorKey(orange4, 0.75f), new GradientColorKey(orange5, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(255, 0.0f), new GradientAlphaKey(255, 1.0f) });

        //Blue
        if (Input.GetKey (KeyCode.A) && GameObject.Find ("Level Script").GetComponent<ColorMatching>().colorMatch == "blue" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = blueGrad;
            main_1.color = blueGrad;
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            float yAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.y, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            float zAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.z, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 blueCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x - 10, GameObject.Find("Main Camera").transform.position.y - 10);
            Vector3 blueAsteroidOffset = new Vector3(GameObject.Find("ReticleBlue").transform.position.x, GameObject.Find("ReticleBlue").transform.position.y, GameObject.Find("ReticleBlue").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, yAngle, zAngle);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, blueCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = blueAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        }
        else if (Input.GetKeyUp (KeyCode.A) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
        //Yellow
		if (Input.GetKey (KeyCode.W) && GameObject.Find("Level Script").GetComponent<ColorMatching>().colorMatch == "yellow" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = yellowGrad;
            main_1.color = yellowGrad;
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 yellowCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x - 10, GameObject.Find("Main Camera").transform.position.y);
            Vector3 yellowAsteroidOffset = new Vector3(GameObject.Find("ReticleYellow").transform.position.x, GameObject.Find("ReticleYellow").transform.position.y, GameObject.Find("ReticleYellow").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, 0, 0);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, yellowCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = yellowAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        }
        else if (Input.GetKeyUp (KeyCode.W) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
        //Red
		if (Input.GetKey (KeyCode.D) && GameObject.Find("Level Script").GetComponent<ColorMatching>().colorMatch == "red" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = redGrad;
            main_1.color = redGrad;
            Debug.Log("HelloWorld");
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            float yAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.y, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            float zAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.z, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 redCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x + 10, GameObject.Find("Main Camera").transform.position.y - 10);
            Vector3 redAsteroidOffset = new Vector3(GameObject.Find("ReticleRed").transform.position.x, GameObject.Find("ReticleRed").transform.position.y, GameObject.Find("ReticleRed").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, yAngle, zAngle);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, redCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = redAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        } else if (Input.GetKeyUp (KeyCode.D) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
        //Green
		if(Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.A) && GameObject.Find("Level Script").GetComponent<ColorMatching>().colorMatch == "green" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = greenGrad;
            main_1.color = greenGrad;
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            float yAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.y, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            float zAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.z, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 greenCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x - 10, GameObject.Find("Main Camera").transform.position.y - 10);
            Vector3 greenAsteroidOffset = new Vector3(GameObject.Find("ReticleGreen").transform.position.x, GameObject.Find("ReticleGreen").transform.position.y, GameObject.Find("ReticleGreen").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, yAngle, zAngle);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, greenCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = greenAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        }
        else if (Input.GetKeyUp (KeyCode.W) && Input.GetKeyUp (KeyCode.A) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
        //Orange
		if(Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.D) && GameObject.Find("Level Script").GetComponent<ColorMatching>().colorMatch == "orange" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = orangeGrad;
            main_1.color = orangeGrad;
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            float yAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.y, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            float zAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.z, -10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 orangeCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x + 10, GameObject.Find("Main Camera").transform.position.y - 10);
            Vector3 orangeAsteroidOffset = new Vector3(GameObject.Find("ReticleOrange").transform.position.x, GameObject.Find("ReticleOrange").transform.position.y, GameObject.Find("ReticleOrange").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, yAngle, zAngle);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, orangeCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = orangeAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        }
        else if (Input.GetKeyUp (KeyCode.W) && Input.GetKeyUp (KeyCode.D) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
        //Magenta
		if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.D) && GameObject.Find("Level Script").GetComponent<ColorMatching>().colorMatch == "magenta" && GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy != 0) {
            float timeDelay = GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay;
            lpfilter.enabled = false;
            main.color = magentaGrad;
            main_1.color = magentaGrad;
            asteroidNumber = GameObject.Find("Level Script").GetComponent<ColorMatching>().asteroidNumber;
            Destroy(GameObject.Find(asteroidNumber), GameObject.Find("Level Script").GetComponent<ColorMatching>().timeDelay);
            //Movement: rotation and position
            float xAngle = Mathf.MoveTowardsAngle(GameObject.Find("Main Camera").transform.eulerAngles.x, 10.0f, timeDelay / 3.07f * Time.deltaTime);
            Vector2 magentaCameraOffset = new Vector2(GameObject.Find("Main Camera").transform.position.x + 10, GameObject.Find("Main Camera").transform.position.y);
            Vector3 magentaAsteroidOffset = new Vector3(GameObject.Find("ReticleMagenta").transform.position.x, GameObject.Find("ReticleMagenta").transform.position.y, GameObject.Find("ReticleMagenta").transform.position.z);
            GameObject.Find("Main Camera").transform.eulerAngles = new Vector3(xAngle, 0, 0);
            GameObject.Find("Main Camera").transform.position = Vector2.MoveTowards(GameObject.Find("Main Camera").transform.position, magentaCameraOffset, step);
            GameObject.Find(asteroidNumber).transform.position = magentaAsteroidOffset;
            GameObject.Find(asteroidNumber).transform.localScale = Vector3.SmoothDamp(GameObject.Find(asteroidNumber).transform.localScale, GameObject.Find(asteroidNumber).transform.localScale / 2, ref velocity, timeDelay / 3.07f);
        }
        else if (Input.GetKeyUp (KeyCode.A) && Input.GetKeyUp (KeyCode.D) || GameObject.Find("Level Script").GetComponent<EnergyMeter>().energy == 0) {
            lpfilter.enabled = true;
            main.color = Color.white;
            main_1.color = Color.white;
        }
    }
}
