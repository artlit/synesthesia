﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorMatching : MonoBehaviour {

	Color32 orange = new Color32(255, 132, 0, 1);
	float timeLeft = 0;
    public float timeDelay;
    public string colorMatch;
    public string asteroidNumber;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		timeLeft += Time.deltaTime;

        //16s
        if (timeLeft > 16 && timeLeft < 20)
        {
            asteroidNumber = "Asteroid (1)";
            timeDelay = 4.0f;
            colorMatch = "red";
            GameObject.Find("Asteroid (1)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (1)").GetComponent<Light>().color = Color.red;
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);

        }
        //Empty
        if (timeLeft > 20 && timeLeft < 24)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //24s
        if (timeLeft > 24 && timeLeft < 28)
        {
            asteroidNumber = "Asteroid (2)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (2)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (2)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //Empty
        if (timeLeft > 28 && timeLeft < 32)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //32s
        if (timeLeft > 32 && timeLeft < 36)
        {
            asteroidNumber = "Asteroid (3)";
            timeDelay = 4.0f;
            colorMatch = "yellow";
            GameObject.Find("Asteroid (3)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (3)").GetComponent<Light>().color = Color.yellow;
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //Empty
        if (timeLeft > 36 && timeLeft < 40)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //40s
        if (timeLeft > 40 && timeLeft < 44)
        {
            asteroidNumber = "Asteroid (4)";
            timeDelay = 4.0f;
            colorMatch = "orange";
            GameObject.Find("Asteroid (4)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (4)").GetComponent<Light>().color = orange;
            GameObject.Find("ReticleOrange").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //44s
        if (timeLeft > 44 && timeLeft < 48)
        {
            asteroidNumber = "Asteroid (5)";
            timeDelay = 4.0f;
            colorMatch = "magenta";
            GameObject.Find("Asteroid (5)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (5)").GetComponent<Light>().color = Color.magenta;
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleOrange").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //48s
        if (timeLeft > 48 && timeLeft < 52)
        {
            asteroidNumber = "Asteroid (6)";
            timeDelay = 4.0f;
            colorMatch = "red";
            GameObject.Find("Asteroid (6)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (6)").GetComponent<Light>().color = Color.red;
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //Empty
        if (timeLeft > 52 && timeLeft < 56)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //56s
        if (timeLeft > 56 && timeLeft < 60)
        {
            asteroidNumber = "Asteroid (7)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (7)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (7)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //Empty
        if (timeLeft > 60 && timeLeft < 64)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //64s
        if (timeLeft > 64 && timeLeft < 68)
        {
            asteroidNumber = "Asteroid (8)";
            timeDelay = 4.0f;
            colorMatch = "magenta";
            GameObject.Find("Asteroid (8)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (8)").GetComponent<Light>().color = Color.magenta;
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //68s
        if (timeLeft > 68 && timeLeft < 72)
        {
            asteroidNumber = "Asteroid (9)";
            timeDelay = 4.0f;
            colorMatch = "green";
            GameObject.Find("Asteroid (9)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (9)").GetComponent<Light>().color = Color.green;
            GameObject.Find("ReticleGreen").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //72s
        if (timeLeft > 72 && timeLeft < 76)
        {
            asteroidNumber = "Asteroid (10)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (10)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (10)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleGreen").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //76s------------------------------8 SEC
        if (timeLeft > 76 && timeLeft < 84)
        {
            asteroidNumber = "Asteroid (11)";
            timeDelay = 8.0f;
            colorMatch = "green";
            GameObject.Find("Asteroid (11)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (11)").GetComponent<Light>().color = Color.green;
            GameObject.Find("ReticleGreen").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //84s
        if (timeLeft > 84 && timeLeft < 88)
        {
            asteroidNumber = "Asteroid (12)";
            timeDelay = 4.0f;
            colorMatch = "red";
            GameObject.Find("Asteroid (12)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (12)").GetComponent<Light>().color = Color.red;
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleGreen").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //88s
        if (timeLeft > 88 && timeLeft < 92)
        {
            asteroidNumber = "Asteroid (13)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (13)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (13)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //92s
        if (timeLeft > 92 && timeLeft < 96)
        {
            asteroidNumber = "Asteroid (14)";
            timeDelay = 4.0f;
            colorMatch = "yellow";
            GameObject.Find("Asteroid (14)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (14)").GetComponent<Light>().color = Color.yellow;
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //96s
        if (timeLeft > 96 && timeLeft < 100)
        {
            asteroidNumber = "Asteroid (15)";
            timeDelay = 4.0f;
            colorMatch = "red";
            GameObject.Find("Asteroid (15)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (15)").GetComponent<Light>().color = Color.red;
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //100s-----------------------------------8 SEC
        if (timeLeft > 100 && timeLeft < 108)
        {
            asteroidNumber = "Asteroid (16)";
            timeDelay = 8.0f;
            colorMatch = "magenta";
            GameObject.Find("Asteroid (16)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (16)").GetComponent<Light>().color = Color.magenta;
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //108s
        if (timeLeft > 108 && timeLeft < 112)
        {
            asteroidNumber = "Asteroid (17)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (17)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (17)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //112s--------------------------------------2 SEC
        if (timeLeft > 112 && timeLeft < 114)
        {
            asteroidNumber = "Asteroid (18)";
            timeDelay = 2.0f;
            colorMatch = "yellow";
            GameObject.Find("Asteroid (18)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (18)").GetComponent<Light>().color = Color.yellow;
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //114s-------------------------------------2 SEC
        if (timeLeft > 114 && timeLeft < 116)
        {
            asteroidNumber = "Asteroid (19)";
            timeDelay = 2.0f;
            colorMatch = "orange";
            GameObject.Find("Asteroid (19)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (19)").GetComponent<Light>().color = orange;
            GameObject.Find("ReticleOrange").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //116s
        if (timeLeft > 116 && timeLeft < 120)
        {
            asteroidNumber = "Asteroid (20)";
            timeDelay = 4.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (20)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (20)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleOrange").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //120s--------------------------------------2 SEC
        if (timeLeft > 120 && timeLeft < 122)
        {
            asteroidNumber = "Asteroid (21)";
            timeDelay = 2.0f;
            colorMatch = "yellow";
            GameObject.Find("Asteroid (21)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (21)").GetComponent<Light>().color = Color.yellow;
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //122s--------------------------------------2 SEC
        if (timeLeft > 122 && timeLeft < 124)
        {
            asteroidNumber = "Asteroid (22)";
            timeDelay = 2.0f;
            colorMatch = "magenta";
            GameObject.Find("Asteroid (22)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (22)").GetComponent<Light>().color = Color.magenta;
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //Empty
        if (timeLeft > 124 && timeLeft < 144)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleMagenta").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //144s
        if (timeLeft > 144 && timeLeft < 148)
        {
            asteroidNumber = "Asteroid (23)";
            timeDelay = 4.0f;
            colorMatch = "red";
            GameObject.Find("Asteroid (23)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (23)").GetComponent<Light>().color = Color.red;
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //Empty
        if (timeLeft > 148 && timeLeft < 152)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleRed").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //152s
        if (timeLeft > 152 && timeLeft < 156)
        {
            asteroidNumber = "Asteroid (24)";
            timeDelay = 4.0f;
            colorMatch = "yellow";
            GameObject.Find("Asteroid (24)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (24)").GetComponent<Light>().color = Color.yellow;
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        //Empty
        if (timeLeft > 156 && timeLeft < 160)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleYellow").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
        //160s----------------------------------8 SEC
        if (timeLeft > 160 && timeLeft < 168)
        {
            asteroidNumber = "Asteroid (25)";
            timeDelay = 8.0f;
            colorMatch = "blue";
            GameObject.Find("Asteroid (25)").GetComponent<Light>().intensity = 3.0f;
            GameObject.Find("Asteroid (25)").GetComponent<Light>().color = Color.blue;
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 2.0f);
        }
        if (timeLeft > 168)
        {
            colorMatch = "empty";
            GameObject.Find("ReticleBlue").GetComponent<Renderer>().material.SetFloat("_MKGlowPower", 0.0f);
        }
    }
}
