﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueAsteroidMove : MonoBehaviour {

    public float speed;

    // Use this for initialization
    void Start()
    {
        speed = 5;
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;
        Vector3 cameraOffset = new Vector3(-3, -2, GameObject.Find("Main Camera").transform.position.z + 40);
        transform.position = Vector3.MoveTowards(transform.position, cameraOffset, step);
    }
}
