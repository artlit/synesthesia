﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyMeter : MonoBehaviour {
    public float energy = 60;
    float maxEnergy = 60;
    bool energyUsed;

    Rect energyRect;
    Rect energyShadow;
    Rect energyRectFlipped;
    Rect energyShadowFlipped;
    Texture2D energyTexture;
    Texture2D shadowTexture;
    Color meterColor = new Color(0, 1, 1, 0.75f);
    Color shadowColor = new Color(0.76f, 0.76f, 0.76f, 0.2f);

    // Use this for initialization
    void Start () {
        energyRect = new Rect(Screen.width * 0.5f, Screen.height * 0.9f, Screen.width * 0.25f, Screen.height * 0.01f);
        energyTexture = new Texture2D(1, 1);
        energyTexture.SetPixel(0, 0, meterColor);
        energyTexture.Apply();
        energyShadow = new Rect(Screen.width * 0.5f, Screen.height * 0.9f, Screen.width * 0.25f, Screen.height * 0.01f);
        shadowTexture = new Texture2D(1, 1);
        shadowTexture.SetPixel(0, 0, shadowColor);
        shadowTexture.Apply();
        //Same but flipped
        energyRectFlipped = new Rect(Screen.width * 0.5f, Screen.height * 0.9f, Screen.width * 0.25f, Screen.height * 0.01f);
        energyShadowFlipped = new Rect(Screen.width * 0.5f, Screen.height * 0.9f, Screen.width * -0.25f, Screen.height * 0.01f);
    }

    void SetEnergyMeter(bool energyUsed) {
        this.energyUsed = energyUsed;
    }

    private void OnGUI()
    {
        float ratio = energy / maxEnergy;
        float rectWidth = ratio * Screen.width * 0.25f;
        energyRect.width = rectWidth;
        energyRectFlipped.width = -rectWidth;
        GUI.DrawTexture(energyShadow, shadowTexture);
        GUI.DrawTexture(energyRect, energyTexture);
        GUI.DrawTexture(energyShadowFlipped, shadowTexture);
        GUI.DrawTexture(energyRectFlipped, energyTexture);
    }

    // Update is called once per frame
    void Update () {
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D)) {
            SetEnergyMeter(true);
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) {
            SetEnergyMeter(false);
        }
        if (energyUsed) {
            energy -= Time.deltaTime;
            if (energy < 0) {
                energy = 0;
                SetEnergyMeter(false);
            }
        } else if (energy < maxEnergy && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D)) {
            energy += (2 * Time.deltaTime);
        }
    }
}
